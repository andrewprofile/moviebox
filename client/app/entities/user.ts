export class User {
    firstName: string;
    lastName: string;
    login: string;
    password: string;
    role: string
}