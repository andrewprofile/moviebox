import { bootstrap } from 'angular2/platform/browser'
import { MainComponent } from './components/main/main'
import { provide, enableProdMode, bind } from 'angular2/core'

import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router'
import {HTTP_PROVIDERS} from 'angular2/http'
import { AuthHttp } from './utils/auth-http'
import { LoginService } from './services/login-service'

bootstrap(MainComponent, [ROUTER_PROVIDERS, bind(LocationStrategy).toClass(HashLocationStrategy), HTTP_PROVIDERS, LoginService, AuthHttp]);