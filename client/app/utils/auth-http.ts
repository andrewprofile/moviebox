import { Http, RequestOptions, XHRBackend, RequestOptionsArgs, Response } from 'angular2/http'
import { Injectable, EventEmitter } from 'angular2/core'
import { LoginService } from '../services/login-service'

@Injectable()
export class AuthHttp extends Http {
    constructor(_backend: XHRBackend, defaultOptions: RequestOptions, private loginService: LoginService) {
        super(_backend, defaultOptions)
    }

    errorEmitter = new EventEmitter(true);

    private addHeadersToOptions(options: RequestOptionsArgs = {}) {
        const headers = {};
        headers['Content-Type'] = 'application/json';
        if (this.loginService.loginInfo.token) {
            headers['JWT'] = this.loginService.loginInfo.token.token
        }
        return Object.assign(options, {
            headers: headers
        })
    }

    private emitEvent(err) {
        if (err.status == 500)
            this.errorEmitter.emit({ message: err.text() })
    }

    get(url: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.get(url, options).do(null, (err: Response) => {
            console.log(err);
            this.emitEvent(err)
        })
    }

    post(url: string, body: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.post(url, body, options).do(null, (err: Response) => {
            this.emitEvent(err)
        })
    }

    put(url: string, body: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.put(url, body, options).do(null, err => {
            this.emitEvent(err)
        })
    }

    delete(url: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.delete(url, options).do(null, err => {
            this.emitEvent(err)
        })
    }

    head(url: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.head(url, options).do(null, err => {
            this.emitEvent(err)
        })
    }

    patch(url: string, body: string, options?: RequestOptionsArgs) {
        options = this.addHeadersToOptions(options);
        return super.patch(url, body, options).do(null, err => {
            this.emitEvent(err)
        })
    }
}