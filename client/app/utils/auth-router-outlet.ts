import {Directive, Attribute, ElementRef, DynamicComponentLoader} from 'angular2/core'
import {Router, RouterOutlet, ComponentInstruction} from 'angular2/router'
import {LoginService} from '../services/login-service'

@Directive({
    selector: 'router-outlet'
})
export class AuthRouterOutlet extends RouterOutlet {
    constructor(private loginService: LoginService, elementRef: ElementRef, loader: DynamicComponentLoader,
        parentRouter: Router, @Attribute('name') nameAttr: string) {
        super(elementRef, loader, parentRouter, nameAttr);
    }

}