import 'angular2/bundles/angular2-polyfills';

require('./vendor/scripts/shims-for-ie.js')

import 'es6-shim'
import 'angular2/platform/browser';
import 'angular2/core';
import 'angular2/http';
import 'angular2/router';
import 'rxjs/Rx';
import 'jquery';
import 'bootstrap';
