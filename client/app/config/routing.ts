import { Route, RouteDefinition } from 'angular2/router';
import { HomeComponent } from '../components/home/home'
import { LoginComponent } from '../components/login/login'
import { RegisterComponent } from "../components/register/register";
import { MovieComponent } from "../components/movie/movie";
import {CategoryComponent} from "../components/category/category";
import {AddComponent} from "../components/add/add";

export class ExtRoute extends Route {
    isPublic: boolean;

    constructor(obj: RouteDefinition, isPublic: boolean = true) {
        super(obj);
        this.isPublic = isPublic
    }
}

export const appRouting: RouteDefinition[] = [
    new ExtRoute({path: "/", name: "Home", component: HomeComponent}, true),
    new ExtRoute({path: "/login", name: "Login", component: LoginComponent}, true),
    new ExtRoute({path: "/register", name: "Register", component: RegisterComponent}, true),
    new ExtRoute({path: "/movie/:id", name: "Movie", component: MovieComponent}, true),
    new ExtRoute({path: "/add/:name", name: "Add", component: AddComponent}, true),
    new ExtRoute({path: "/category/:name", name: "Category", component: CategoryComponent}, true)
];
