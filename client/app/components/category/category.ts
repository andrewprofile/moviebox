import { Component } from 'angular2/core';
import {HomeService} from "../../services/home-service";
import {NgZone} from "angular2/src/core/zone/ng_zone";
import {RouteParams} from "angular2/src/router/instruction";

class Movie
{
    description: string
}

@Component({
    template: require("./category.tpl.html"),
    providers: [HomeService]
})
export class CategoryComponent {
    movies: Array<Movie>;
    name: string;
    constructor(private homeService: HomeService, private params: RouteParams, private zone: NgZone) {
        zone.run(() => {
            if(this.params.get('name')) {
                this.showAll(this.params.get('name'));
                this.name = CategoryComponent.capitalizeFirstLetter(this.params.get('name'));
            }
        })
    }

    showAll(name: string) {
        switch(name)
        {
            default:
            case "movie":
                this.homeService.showAllMovie().then(res => {
                    this.zone.run(() => {
                        this.movies = res;
                        for(let i=0; i< this.movies.length; i++) {
                            this.movies[i].description = this.movies[i].description.substring(0, 303)
                        }
                    })
                }, rej => {
                    this.movies = null
                });
                break;
            case 'tv':
                this.homeService.showAllTv().then(res => {
                    this.zone.run(() => {
                        this.movies = res;
                        for(let i=0; i< this.movies.length; i++) {
                            this.movies[i].description = this.movies[i].description.substring(0, 303)
                        }
                    })
                }, rej => {
                    this.movies = null
                });
                break;
            case 'game':
                this.homeService.showAllGame().then(res => {
                    this.zone.run(() => {
                        this.movies = res;
                        for(let i=0; i< this.movies.length; i++) {
                            this.movies[i].description = this.movies[i].description.substring(0, 303)
                        }
                    })
                }, rej => {
                    this.movies = null
                });
                break;
        }
    }

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

