import { Component, NgZone } from 'angular2/core';
import { LoginService } from '../../services/login-service'
import { Router, RouterLink } from 'angular2/router'
import { FaComponent } from 'angular2-fontawesome'

require('./login.scss');

@Component({
    template: require('./login.tpl.html'),
    directives: [RouterLink, FaComponent]
})   
export class LoginComponent {
    isError: boolean;
    submitted: boolean;

    constructor(private loginService: LoginService, public router: Router, private zone: NgZone) {
        zone.run(() => {
            this.submitted = false;
            this.isError = false
        })
    }
    
    login(username, password) {
        this.submitted = true;
        this.isError = false;

        this.loginService.login(username, password).then(res => {
            this.zone.run(() => {
                this.submitted = false;
                if (!res) {
                    this.isError = true
                }
            })
        }, rej => {
            this.submitted = false;
            this.isError = true
        })
    }
}
