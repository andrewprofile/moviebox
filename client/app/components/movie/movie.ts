import { Component, NgZone } from 'angular2/core';
import { LoginService } from '../../services/login-service'
import { Router, RouterLink, RouteParams } from 'angular2/router'
import { FaComponent } from 'angular2-fontawesome'
import {MovieService} from "../../services/movie-service";

require('./movie.scss');

class Rate {
    Rate: number
}

class Comment {
    userId: number;
    username: string
}

@Component({
    template: require('./movie.tpl.html'),
    providers: [MovieService],
    directives: [RouterLink, FaComponent]
})   
export class MovieComponent {
    isError: boolean;
    submitted: boolean;
    movie: Object;
    comments: Array<Comment>;
    model;
    rating: boolean;
    rates;
    rate;

    constructor(private loginService: LoginService, private params: RouteParams, private movieService: MovieService, public router: Router, private zone: NgZone) {
        zone.run(() => {
            this.submitted = false;
            this.isError = false;
            this.model = new Rate();
            this.model.Rate = 1;
            this.rating = false;
            this.rates = [];

            if(+this.params.get('id')) {
                this.getMovie(+this.params.get('id'));
                this.getMovieComments(+this.params.get('id'));
                this.getAvgOfRate(+this.params.get('id'));
                if(this.loginInfo.isLoggedIn) {
                    this.getRate(+this.params.get('id'), this.loginInfo.userId)
                }

            }
        })
    }

    get loginInfo() {
        return this.loginService.loginInfo
    }

    getMovie(id: number) {
        this.movieService.showMovie(id).then(res => {
            this.zone.run(() => {
                this.movie = res.item;
                console.log(this.movie);
            })
        }, rej => {
            this.movie = null;
            return rej
        })
    }

    getMovieComments(id: number) {
        this.movieService.showMovieComments(id).then(res => {
            this.zone.run(() => {
                this.comments = res.items;
                if(this.comments == null) return null;
                this.submitted = res.items;
                for(let i=0; i< this.comments.length; i++){
                     this.getUser(this.comments[i].userId, i);
                }
                console.log(this.submitted);
            })
        }, rej => {
            this.comments = null;
            return rej
        })
    }

    addComment(message: string, movieId: number, userId: number) {
        this.movieService.addComment(message, movieId, userId).then(res => {
            this.zone.run(() => {
                console.log(res);
            })
        }, rej => {
            return rej
        })
    }

    addRate(rate:number, movieId: number, userId: number) {
        this.movieService.addRate(rate, movieId, userId).then(res => {
            this.zone.run(() => {
                console.log(res);
            })
        }, rej => {
            return rej
        })
    }

    setRate(element: HTMLInputElement) {
        this.model.Rate = element.value;
    }

    getAvgOfRate(movieId: number) {
        this.movieService.getAvgRate(movieId).then(res => {
            this.zone.run(() => {
                this.rate = res.item.rating;
            })
        }, rej => {
            return rej
        })
    }

    getRate(movieId: number, userId: number) {
        this.movieService.getRate(movieId, userId).then(res => {
            this.zone.run(() => {
                if(res.item.rating === 0) return null;
                console.log(res.item);
                this.rating = true;
                for(let i=0; i< res.item.rating; i++){
                    this.rates.push(0);
                }
            })
        }, rej => {
            this.rating = false;
            return rej
        })
    }

    getUser(userId: number, id: number){
        this.movieService.getUser(userId).then(res => {
            this.zone.run(() => {
                    this.comments[id].username = res.item.username;
                    console.log(res.item.username);
            });
        }, rej => {
            return rej
        })
    }


}
