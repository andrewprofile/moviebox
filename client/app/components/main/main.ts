import { Component } from 'angular2/core';
import { RouteConfig, Router, RouterOutlet } from 'angular2/router'
import { appRouting } from '../../config/routing'
import { LoginService } from '../../services/login-service'
import { RouterLink } from "angular2/src/router/directives/router_link";

require('bootstrap/dist/css/bootstrap.min.css');
require('font-awesome/scss/font-awesome.scss');
require('./main.scss');

@Component({
    selector: 'main',
    template: require("./main.tpl.html"),
    directives: [ RouterOutlet, RouterLink]
})
@RouteConfig(appRouting)
export class MainComponent {

    constructor(private router: Router, private loginService: LoginService) {

    }

    get loginInfo() {
        return this.loginService.loginInfo
    }

    goToLogin() {
        this.router.navigate(['Login'])
    }

    logout() {
        this.loginService.logout()
    }
}
