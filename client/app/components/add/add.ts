import { Component } from 'angular2/core';
import {NgZone} from "angular2/src/core/zone/ng_zone";
import {RouteParams} from "angular2/src/router/instruction";
import {AddService} from "../../services/add-service";


@Component({
    template: require("./add.tpl.html"),
    providers: [AddService]
})
export class AddComponent {
    name: string;
    constructor(private addService: AddService, private params: RouteParams, private zone: NgZone) {
        zone.run(() => {
            if(this.params.get('name')) {
                this.name = AddComponent.capitalizeFirstLetter(this.params.get('name'));
            }
        });
    }

    addMovie(title: string, description: string, duration: string, path: string) {
        switch(this.name)
        {
            default:
            case "Movie":
                this.addService.addMovie(title, description, duration, path, 1).then(res => {
                    this.zone.run(() => {
                        console.log(res);
                    })
                }, rej => {
                    return rej
                });
                break;
            case 'Tv':
                this.addService.addMovie(title, description, duration, path, 2).then(res => {
                    this.zone.run(() => {
                        console.log(res);
                    })
                }, rej => {
                    return rej
                });
                break;
            case 'Game':
                this.addService.addMovie(title, description, duration, path, 3).then(res => {
                    this.zone.run(() => {
                        console.log(res);
                    })
                }, rej => {
                    return rej
                });
                break;
        }
    }

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

