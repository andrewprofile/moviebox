import { Component } from 'angular2/core';
import {NgZone} from "angular2/src/core/zone/ng_zone";
import {HomeService} from "../../services/home-service";

class Movie {
    id: number
}

@Component({
    template: require("./home.tpl.html"),
    providers: [HomeService]
})
export class HomeComponent {
    movies: Array<Object>;
    tvs: Array<Object>;
    constructor(private homeService: HomeService, private zone: NgZone) {
        zone.run(() => {
            this.showAll()
        })
    }

    showAll() {
        this.homeService.showAllMovie().then(res => {
            this.zone.run(() => {
                this.movies = res;
                this.movies = this.movies.filter(HomeComponent.filterMovie)

            })
        }, rej => {
            this.movies = null
        });
        this.homeService.showAllTv().then(res => {
            this.zone.run(() => {
                this.tvs = res;
                this.tvs = this.tvs.filter(HomeComponent.filterTv)
                console.log(this.tvs)
            })
        }, rej => {
            this.tvs = null
        })
    }

    static filterMovie(item: Movie) {
        return item.id < 5;
    }

    static filterTv(item: Movie) {
        return item.id < 10;
    }
}

