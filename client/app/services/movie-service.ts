import { Injectable } from 'angular2/core'
import { AuthHttp } from '../utils/auth-http'
import { Variables } from '../config/config'
import { Router } from "angular2/src/router/router";

const API_URL = Variables.API_URL;

@Injectable()
export class MovieService {
    constructor(private http: AuthHttp, private router: Router) { }

    showMovie(id: number) {
        return this.http.get(API_URL + 'api/movie/'+id).toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    showMovieComments(id: number) {
        return this.http.get(API_URL + 'api/comment/'+id+'/movie').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    addComment(message: string, movieId: number, userId: number) {
        const body = {
            'message': message,
            'movieId': movieId,
            'userId':  userId
        };

        return this.http.put(API_URL + 'api/comment', JSON.stringify(body)).toPromise()
            .then(res => {
                window.location.reload();
                return res;
            }, rej => {
                return null
            })
    }

    addRate(rate:number, movieId: number, userId: number) {
        const body = {
            'rating': rate,
            'movieId': movieId,
            'userId':  userId
        };

        return this.http.put(API_URL + 'api/rate', JSON.stringify(body)).toPromise()
            .then(res => {
                window.location.reload();
                return res;
            }, rej => {
                return null
            })
    }

    getAvgRate(movieId: number) {
        return this.http.get(API_URL + 'api/rate/'+movieId+'/movie').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    getRate(movieId: number, userId: number) {
        return this.http.get(API_URL + 'api/rate/'+movieId+'/movie/'+userId).toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    getUser(userId: number) {
        return this.http.get(API_URL + 'api/account/'+userId).toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }
}