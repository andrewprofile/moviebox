import { Injectable } from 'angular2/core'
import { AuthHttp } from '../utils/auth-http'
import { Variables } from '../config/config'
import { Router } from "angular2/src/router/router";

const API_URL = Variables.API_URL;

@Injectable()
export class AddService {
    constructor(private http: AuthHttp, private router: Router) { }

    addMovie(title: string, description: string, duration: string, path: string, categoryId: number) {
        const body = {
            'title': title,
            'description': description,
            'duration':  duration,
            'language': 'English',
            'path': path,
            'categoryId': categoryId
        };

        return this.http.put(API_URL + 'api/movie', JSON.stringify(body)).toPromise()
            .then(res => {
                this.router.navigate(['Home']);
                return res;
            }, rej => {
                return null
            })
    }
}