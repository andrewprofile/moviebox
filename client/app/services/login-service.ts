import { Injectable } from 'angular2/core'
import { Http } from 'angular2/http'
import { contentHeaders } from '../helpers/headers'
import { Router } from 'angular2/router'
import { Variables } from '../config/config'

const API_URL = Variables.API_URL;

class Token {
    token: string;
    userName: string;
    userId: number;
    role: string
}

class LoginInfo {
    token: Token;

    get isLoggedIn() {
        return !(!this.token)
    }

    get userName() {
        return this.isLoggedIn ? this.token.userName : null
    }

    get role() {
        return this.token.role ? this.token.role : null
    }

    get userId() {
        return this.token.userId ? this.token.userId : null
    }
}

@Injectable()
export class LoginService {
    loginInfo = new LoginInfo();

    constructor(public http: Http, private router: Router) {
        const tokenJson = localStorage.getItem('token');
        if(tokenJson)
            this.loginInfo.token = JSON.parse(tokenJson)
    }

    login(username: string, password: string) {
        const body = {
            'username': username,
            'password': password
        };

        return this.http.post(API_URL + 'api/account', JSON.stringify(body), { headers: contentHeaders }).toPromise()
            .then(res => {
                if (!res.json())
                    return null;
                
                this.loginInfo.token = new Token();
                this.loginInfo.token.userId = res.json().id;
                this.loginInfo.token.userName = res.json().username;
                this.loginInfo.token.token = res.json().token;
                localStorage.setItem('token', JSON.stringify(this.loginInfo.token));
                this.router.navigate(['Home']);

                return res.json()
            }, rej => {
                return null
            })
    }

    logout() {
        localStorage.removeItem('token');
        this.loginInfo.token = null;
        this.router.navigate(['Login'])
    }
}