import { Injectable } from 'angular2/core'
import { AuthHttp } from '../utils/auth-http'
import { Router } from 'angular2/router'
import { Variables } from '../config/config'

const API_URL = Variables.API_URL;

@Injectable()
export class HomeService {
    constructor(private http: AuthHttp, private router: Router) { }

    showAllMovie() {
        return this.http.get(API_URL + 'api/movie').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    showAllTv() {
        return this.http.get(API_URL + 'api/movie/tv').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    showAllGame() {
        return this.http.get(API_URL + 'api/movie/game').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

    getAvgRate(movieId: number) {
        return this.http.get(API_URL + 'api/rate/'+movieId+'/movie').toPromise()
            .then(res => {
                if (!res.json())
                    return null;

                return res.json()
            }, rej => {
                return null
            })
    }

}