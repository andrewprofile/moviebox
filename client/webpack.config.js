var path = require('path');
var webpack = require('webpack');

var CopyWebpackPlugin = require('copy-webpack-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: __dirname,
    entry: {
        app: './app/entry.ts',
        vendor: './app/vendor.ts'
    },
    output: {
        filename: '../bundle.js',
        publicPath: '/assets/',
        path: './build/client/assets'
    },
    resolve: {
        root: [path.join(__dirname, "node_modules"), path.join(__dirname, "bower_components")],
        extensions: ['', '.ts', '.js']
    },
    devtool: "#source-map",
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: '../vendor.js', minChunks: Infinity}),
        new webpack.optimize.CommonsChunkPlugin({name: 'common', filename: '../common.js', minChunks: 2, chunks: ['app', 'vendor']}),
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('.bower.json', ['main'])
        ),
        new CopyWebpackPlugin([
            {
                from: './assets',
                to: 'assets'
            }
        ]),
        new ExtractTextPlugin('css/style.css'),
        new ProgressBarPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts' },
            { test: /\.html$/, loader: 'raw' },
            { test: /\.scss$/, loaders: ['style', 'css', 'sass'] },
            { test: /\.css$/, loaders: ['style', 'css'] },
            { test: /\.js$/, loader: 'script', exclude: /node_modules/ },
            { test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file?hash=sha512&digest=hex&publicPath=../&name=images/[hash].[ext]' },
            { test: /\.(eot|woff|woff2|ttf|svg).*$/, loader: 'file' }
        ],
    }
}