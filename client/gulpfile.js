var gulp = require("gulp")
var typings = require("gulp-typings");
var glob = require("glob")
var fs = require("fs")
var webpack = require("webpack")
var spawn = require("child_process").spawn
var seq = require("run-sequence")
var ts = require("gulp-typescript")
var mocha = require('gulp-mocha')
var sourcemaps = require("gulp-sourcemaps")
var livereload = require('livereload')
var connect = require('gulp-connect')

var node

var webpackProgress = function(err, stats, next) {
    if(err) {
        console.log("ERROR: " + err)
        return
    }
    
    var jsonStats = stats.toJson()
    if(jsonStats.errors.length > 0) {
        console.log("ERROR: " + jsonStats.errors)
        return
    }
    
    if(jsonStats.warnings.length > 0)
        console.log("WARNING: " + jsonStats.warnings)
    
    if(next) next()
}

gulp.task('build:index.html', next => {
    return gulp.src(["./index.html"])
        .pipe(gulp.dest('./build/client'))
})

gulp.task('build:webpack', next => {
    var compiler = webpack(require('./webpack.config'))
    compiler.run((err, stats) => next())
})

var webpackComplete = false
gulp.task('watch:webpack', next => {
    var compiler = webpack(require('./webpack.config'))
    compiler.watch(null, (err, stats) => webpackProgress(err, stats, () => {
        if(!webpackComplete) {
            webpackComplete = true
            next()
        }
    }))
})

gulp.task('server', next => {
    connect.server({
        root: './build/client',
        port: 3001
    })
    next()
})

gulp.task('test', () => {
    return gulp.src('build/server/tests/tests.js', { read: false })
        .pipe(mocha({reporter: 'nyan', bail: true }))
})

gulp.task('sync', ['watch:webpack'], () => {
    server = livereload.createServer()
    server.watch(__dirname + "/build/client")
    console.log("Build complete, sync is active now...")
})

gulp.task('build', ['build:webpack'])
gulp.task('watch', ['sync'])

gulp.task('default', _ => seq('build:index.html', 'server', 'test', 'watch'))

process.on('exit', function () {
    if (node) node.kill()
})