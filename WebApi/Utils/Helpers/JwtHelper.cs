﻿using System.Collections.Generic;

namespace WebApi.Utils.Helpers
{
    public class JwtHelper
    {
        private const string SecretKey = "MACHINE_KEY_LUB_INNY_BEZPIECZNY_KLUCZ";

        public static string EncodeToken(Dictionary<string, object> payload)
        {
            return Jwt.JsonWebToken.Encode(payload, SecretKey, Jwt.JwtHashAlgorithm.HS256);
        }

        public static Dictionary<string, object> DecodeToken(string token)
        {
            return Jwt.JsonWebToken.DecodeToObject<Dictionary<string, object>>(token, SecretKey);
        }
    }
}