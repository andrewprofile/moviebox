﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace WebApi.Utils
{
    public class WebServiceExceptionAttribute : ExceptionFilterAttribute
    {
        private static void HandleException(ExceptionContext context)
        {
            if (context.Exception is StatusCodeException) return;

            context.Result = new ContentResult
            {
                ContentType = "application/json",
                Content     = JsonConvert.SerializeObject(new { Message = "Service Unavailable" }),
                StatusCode  = (int)HttpStatusCode.ServiceUnavailable
            };
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);
            base.OnException(context);
        }

        public override Task OnExceptionAsync(ExceptionContext context)
        {
            HandleException(context);

            return base.OnExceptionAsync(context);
        }
    }
}