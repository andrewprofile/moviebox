﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Utils.Base.Interfaces;

namespace WebApi.Utils.Base
{
    public class BaseController<T, TU> : Controller where T : BasicEntity where TU : IBasicEntity
    {
        protected IBaseService<T> Service;

        public BaseController(IBaseService<T> service)
        {
            Service = service;
        }

        [HttpGet("{id}")]
        public virtual BaseViewModel<T> Get(int id)
        {
            return Service.Get(id);
        }

        [HttpPost]
        public virtual BaseViewModel Post([FromBody] TU item)
        {
            Service.IsValidModel(ModelState);

            return Service.Post(Service.CreateEntityFromViewModel(item));
        }

        [HttpPut]
        public virtual void Put([FromBody] TU item)
        {
            Service.IsValidModel(ModelState);
            Service.Put(Service.CreateEntityFromViewModel(item));
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Service.Delete(id);
        }
    }
}