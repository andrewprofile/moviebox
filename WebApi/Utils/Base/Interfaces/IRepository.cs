﻿using System.Collections.Generic;

namespace WebApi.Utils.Base.Interfaces
{
    public interface IRepository<T> where T : BasicEntity
    {
        IEnumerable<T> GetAll();
        T              Get(long id);
        void           Insert(T entity);
        void           Update(T entity);
        void           Delete(T entity);
        void           Save();
    }
}