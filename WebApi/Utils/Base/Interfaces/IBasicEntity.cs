﻿namespace WebApi.Utils.Base.Interfaces
{
    public interface IBasicEntity
    {
        long Id { get; set; }
    }
}