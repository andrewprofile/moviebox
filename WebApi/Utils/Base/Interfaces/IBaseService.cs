﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WebApi.Utils.Base.Interfaces
{
    public interface IBaseService<T> where T : BasicEntity
    {
        T                CreateEntityFromViewModel(IBasicEntity item);
        BaseViewModel<T> Get(int id);
        void             Delete(int id);
        BaseViewModel    Post(T item);
        void             Put(T item);
        bool             IsValidModel(ModelStateDictionary modelState);
    }
}