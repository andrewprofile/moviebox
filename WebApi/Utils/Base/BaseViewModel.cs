﻿namespace WebApi.Utils.Base
{
    public class BaseViewModel<T> where T : BasicEntity
    {
        public T Item { get; set; }
    }

    public class BaseViewModel
    {
        public long Id { get; set; }
    }
}