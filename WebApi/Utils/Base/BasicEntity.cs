﻿using WebApi.Utils.Base.Interfaces;

namespace WebApi.Utils.Base
{
    public class BasicEntity : IBasicEntity
    {
        public long Id { get; set; }
    }
}