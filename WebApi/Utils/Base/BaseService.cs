﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using WebApi.Utils.Base.Interfaces;

namespace WebApi.Utils.Base
{
    public abstract class BaseService<T> : IBaseService<T> where T : BasicEntity
    {
        protected readonly IRepository<T> Repository;

        protected BaseService(IRepository<T> repository)
        {
            Repository = repository;
        }

        public abstract T CreateEntityFromViewModel(IBasicEntity item);

        public virtual BaseViewModel<T> Get(int id)
        {
            var item = Repository.Get(id);

            if (item == null)
            {
                throw StatusCodeException.NotAcceptable();
            }

            return new BaseViewModel<T>
            {
                Item = item
            };
        }

        public void Delete(int id)
        {
            Repository.Delete(Get(id).Item);
            Repository.Save();
        }

        public virtual BaseViewModel Post(T item)
        {
            Repository.Update(item);
            Repository.Save();

            return new BaseViewModel
            {
                Id   = item.Id
            };
        }

        public virtual void Put(T item)
        {
            Repository.Insert(item);
            Repository.Save();
        }

        public bool IsValidModel(ModelStateDictionary modelState)
        {
            if (modelState.IsValid)
            {
                return true;
            }

            throw StatusCodeException.NotAcceptable(typeof(T).Name);
        }
    }
}