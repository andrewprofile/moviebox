﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApi.Utils.Base.Interfaces;

namespace WebApi.Utils.Base
{
    public class Repository<T> : IRepository<T> where T : BasicEntity
    {
        protected readonly DbContext Context;
        protected readonly DbSet<T>  Entities;

        public Repository(DbContext context)
        {
            Context  = context;
            Entities = context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return Entities.AsEnumerable();
        }

        public T Get(long id)
        {
            return Entities.SingleOrDefault(s => s.Id == id);
        }

        public void Insert(T entity)
        {
            Entities.Add(entity);
        }

        public void Update(T entity)
        {
            Entities.Update(entity);
        }

        public void Delete(T entity)
        {
            Entities.Remove(entity);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}