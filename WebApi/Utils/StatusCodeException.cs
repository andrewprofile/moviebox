﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace WebApi.Utils
{
    public class StatusCodeExceptionAttribute : ExceptionFilterAttribute
    {
        private static void HandleException(ExceptionContext context)
        {
            if (!(context.Exception is StatusCodeException)) return;

            var statusCodeException = (StatusCodeException)context.Exception;

            context.Result = new ContentResult
            {
                ContentType = "application/json",
                Content     = JsonConvert.SerializeObject(new { Message = statusCodeException.StatusMessage }),
                StatusCode  = statusCodeException.StatusCode
            };
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);
            base.OnException(context);
        }

        public override Task OnExceptionAsync(ExceptionContext context)
        {
            HandleException(context);

            return base.OnExceptionAsync(context);
        }
    }

    public class StatusCodeException : System.Exception
    {
        public int    StatusCode    { get; set; }
        public string StatusMessage { get; set; }

        public StatusCodeException(int statusCode, string statusMessage = "")
        {
            StatusCode    = statusCode;
            StatusMessage = statusMessage;
        }

        public static StatusCodeException NotAcceptable(string message = "User")
        {
            return new StatusCodeException((int)HttpStatusCode.NotAcceptable, "Missing "+ message +" data");
        }

        public static StatusCodeException CurrentUserExist()
        {
            return new StatusCodeException((int)HttpStatusCode.NotAcceptable, "Current User exist");
        }

        public static StatusCodeException Unauthorized()
        {
            return new StatusCodeException((int)HttpStatusCode.Unauthorized, "Unconfirmed User");
        }
    }
}