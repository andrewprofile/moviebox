﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace WebApi.Utils
{
    public class Common
    {
        public static string Sha256(string text)
        {
            var bytes      = Encoding.UTF8.GetBytes(text);
            var hashString = SHA256.Create();
            var hash       = hashString.ComputeHash(bytes);

            return hash.Aggregate(string.Empty, (current, x) => current + $"{x:x2}");
        }
    }
}