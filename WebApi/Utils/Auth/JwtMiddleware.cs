﻿using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace WebApi.Utils.Auth
{
    public class JwtMiddleware : AuthenticationMiddleware<JwtOptions>
    {
        public JwtMiddleware(
            RequestDelegate next,
            IOptions<JwtOptions> options,
            ILoggerFactory loggerFactory
        ) : base(next, options, loggerFactory, UrlEncoder.Create())
        {
        }

        protected override AuthenticationHandler<JwtOptions> CreateHandler()
        {
            return new JwtHandler();
        }
    }
}