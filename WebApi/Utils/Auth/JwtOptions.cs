﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace WebApi.Utils.Auth
{
    public class JwtOptions : AuthenticationOptions, IOptions<JwtOptions>
    {
        public JwtOptions Value => this;

        public JwtOptions()
        {
            AuthenticationScheme  = "JWT";
            AutomaticAuthenticate = true;
            AutomaticChallenge    = true;
        }
    }
}