﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using WebApi.Utils.Helpers;

namespace WebApi.Utils.Auth
{
    public class ApplicationUser : IIdentity
    {
        public ApplicationUser(string authenticationType, bool isAuthenticated, string name)
        {
            AuthenticationType = authenticationType;
            IsAuthenticated    = isAuthenticated;
            Name               = name;
        }

        public string AuthenticationType { get; }

        public bool   IsAuthenticated    { get; }

        public string Name               { get; }
    }

    public class JwtHandler : AuthenticationHandler<JwtOptions>
    {
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var tokenStrings = Request.Headers["JWT"];

            if (!tokenStrings.Any())
            {
                return Task.FromResult(AuthenticateResult.Fail("No JWT token provided"));
            }

            try
            {
                var payload                  = JwtHelper.DecodeToken(tokenStrings[0]);
                var claimsPrincipal          = new ClaimsPrincipal(
                    new ApplicationUser(Options.AuthenticationScheme, true, payload["username"].ToString())
                );
                var authenticationProperties = new AuthenticationProperties();
                var authenticationTicket     = new AuthenticationTicket(
                    claimsPrincipal, authenticationProperties, Options.AuthenticationScheme
                );
                var result                   = AuthenticateResult.Success(authenticationTicket);

                return Task.FromResult(result);
            }
            catch (Exception)
            {
                var result = AuthenticateResult.Fail("Invalid JWT token");

                return Task.FromResult(result);
            }
        }
    }
}