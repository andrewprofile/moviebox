﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Models.RateViewModel
{
    public class RateViewModel : BasicEntity
    {
        [Required]
        public long?  MovieId { get; set; }
        [Required]
        public double Rating  { get; set; }
        [Required]
        public long?  UserId  { get; set; }
    }
}