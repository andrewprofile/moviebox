﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base;

namespace MovieBoxApi.Models.CommentViewModel
{
    public class CommentsViewModel : BaseViewModel<Comment>
    {
        public IEnumerable<Comment> Items { get; set; }
    }
}