﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Models.CommentViewModel
{
    public class CommentViewModel : BasicEntity
    {
        [Required]
        public long?  MovieId { get; set; }
        [Required]
        public long?  UserId  { get; set; }
        [Required]
        public string Message { get; set; }
    }
}