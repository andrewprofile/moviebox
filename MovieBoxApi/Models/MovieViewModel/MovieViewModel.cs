﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Models.MovieViewModel
{
    public class MovieViewModel : BasicEntity
    {
        [Required]
        public string               Title       { get; set; }
        [Required]
        public string               Description { get; set; }
        public string               Language    { get; set; }
        public double               Duration    { get; set; }
        public string               Path        { get; set; }
        public long                 CategoryId  { get; set; }
    }
}