﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Models.AccountViewModel
{
    public class UserViewModel : BasicEntity
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}