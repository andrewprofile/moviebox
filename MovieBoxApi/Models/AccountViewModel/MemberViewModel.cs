﻿using WebApi.Utils.Base;

namespace MovieBoxApi.Models.AccountViewModel
{
    public class MemberViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public string Token    { get; set; }
    }
}