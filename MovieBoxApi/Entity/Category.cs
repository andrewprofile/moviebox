﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Entity
{
    public class Category : BasicEntity
    {
        [Required]
        public string Name { get; set; }
    }
}