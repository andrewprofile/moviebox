﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Entity
{
    public class Comment : BasicEntity
    {
        [Required]
        public string Message { get; set; }
        [Required]
        public long?  MovieId { get; set; }
        [Required]
        public long?  UserId  { get; set; }
    }
}