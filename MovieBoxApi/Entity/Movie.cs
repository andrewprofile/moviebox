﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Entity
{
    public class Movie : BasicEntity
    {
        [Required]
        public string               Title       { get; set; }
        [Required]
        public string               Description { get; set; }
        public string               Language    { get; set; }
        public double               Duration    { get; set; }
        public string               Path        { get; set; }
        public long                 CategoryId  { get; set; }
        public IEnumerable<Rate>    Rating      { get; set; }
        public IEnumerable<Comment> Comments    { get; set; }
    }
}