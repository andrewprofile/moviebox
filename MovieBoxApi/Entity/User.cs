using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Entity
{
    public class User : BasicEntity
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}