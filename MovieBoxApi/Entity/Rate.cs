﻿using System.ComponentModel.DataAnnotations;
using WebApi.Utils.Base;

namespace MovieBoxApi.Entity
{
    public class Rate : BasicEntity
    {
        [Required]
        public long?  MovieId { get; set; }
        [Required]
        public double Rating  { get; set; }
        [Required]
        public long?  UserId  { get; set; }
    }
}