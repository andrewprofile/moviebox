﻿using System.Collections.Generic;
using System.Linq;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.CommentViewModel;
using MovieBoxApi.Repository.Interfaces;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services
{
    public class CommentService : BaseService<Comment>, ICommentService
    {
        protected readonly ICommentRepository CommentRepository;

        public CommentService(ICommentRepository repository) : base(repository)
        {
            CommentRepository = repository;
        }

        public override Comment CreateEntityFromViewModel(IBasicEntity item)
        {
            var model = (CommentViewModel) item;

            return new Comment
            {
                Id      = model.Id,
                Message = model.Message,
                MovieId = model.MovieId,
                UserId  = model.UserId
            };
        }

        public IEnumerable<Comment> GetAll()
        {
            return Repository.GetAll();
        }

        public BaseViewModel<Comment> GetByMovieId(long id)
        {
            return new CommentsViewModel
            {
                Item  = null,
                Items = CommentRepository.GetByMovieId(id).Count() != 0 ? CommentRepository.GetByMovieId(id) : null
            };
        }
    }
}