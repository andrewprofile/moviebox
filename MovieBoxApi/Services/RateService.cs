﻿using MovieBoxApi.Entity;
using MovieBoxApi.Models.RateViewModel;
using MovieBoxApi.Repository.Interfaces;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services
{
    public class RateService : BaseService<Rate>, IRateService
    {
        protected readonly IRateRepository RateRepository;

        public RateService(IRateRepository repository) : base(repository)
        {
            RateRepository = repository;
        }

        public override Rate CreateEntityFromViewModel(IBasicEntity item)
        {
            var model = (RateViewModel) item;

            return new Rate
            {
                Id      = model.Id,
                MovieId = model.MovieId,
                Rating  = model.Rating,
                UserId = model.UserId
            };
        }

        public BaseViewModel<Rate> GetByMovieId(int id)
        {
            var item = new Rate
            {
                Id      = 1,
                MovieId = id,
                Rating  = RateRepository.GetAvgByMovieId(id),
                UserId  = null
            };

            return new BaseViewModel<Rate>
            {
                Item = item
            };
        }

        public BaseViewModel<Rate> GetByMovieIdAndUserId(int id, int userId)
        {
            var item = new Rate
            {
                Id      = 1,
                MovieId = id,
                Rating  = RateRepository.GetByMovieIdAndUserId(id, userId),
                UserId  = userId
            };

            return new BaseViewModel<Rate>
            {
                Item = item
            };
        }
    }
}