﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services
{
    public class CategoryService : BaseService<Category>, ICategoryService
    {
        public CategoryService(IRepository<Category> repository) : base(repository)
        {
        }

        public IEnumerable<Category> GetAll()
        {
            return Repository.GetAll();
        }

        public override Category CreateEntityFromViewModel(IBasicEntity item)
        {
            throw new System.NotImplementedException();
        }
    }
}