﻿using System;
using System.Collections.Generic;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.AccountViewModel;
using MovieBoxApi.Repository.Interfaces;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;
using WebApi.Utils.Helpers;

namespace MovieBoxApi.Services
{
    public class UserService : BaseService<User>, IUserService
    {
        protected readonly IUserRepository UserRepository;

        public UserService(IUserRepository repository) : base(repository)
        {
            UserRepository = repository;
        }

        public override User CreateEntityFromViewModel(IBasicEntity item)
        {
            var model = (UserViewModel) item;

            return new User
            {
                Username = model.Username,
                Password = Common.Sha256(model.Password)
            };
        }

        public override BaseViewModel Post(User item)
        {
            var user = UserRepository.Get(item);

            if (user == null)
            {
                throw StatusCodeException.Unauthorized();
            }

            return new MemberViewModel
            {
                Id       = user.Id,
                Username = user.Username,
                Token    = JwtHelper.EncodeToken(CreatePayload(user))
            };
        }

        public override void Put(User item)
        {
            if (UserRepository.GetByName(item.Username) != null)
            {
                throw StatusCodeException.CurrentUserExist();
            }

            UserRepository.Insert(item);
            UserRepository.Save();
        }

        private static Dictionary<string, object> CreatePayload(User user)
        {
            return new Dictionary<string, object>
            {
                { "username",  user.Username },
                { "timestamp", DateTime.Now  }
            };
        }
    }
}