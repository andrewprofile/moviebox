﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services.Interfaces
{
    public interface IMovieService : IBaseService<Movie>
    {
        IEnumerable<Movie> GetAllMovie();
        IEnumerable<Movie> GetAllTV();
        IEnumerable<Movie> GetAllGame();
    }
}