﻿using MovieBoxApi.Entity;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services.Interfaces
{
    public interface IRateService : IBaseService<Rate>
    {
        BaseViewModel<Rate> GetByMovieId(int id);
        BaseViewModel<Rate> GetByMovieIdAndUserId(int id, int userId);
    }
}