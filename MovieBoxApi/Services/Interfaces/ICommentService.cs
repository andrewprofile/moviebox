﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services.Interfaces
{
    public interface ICommentService : IBaseService<Comment>
    {
        IEnumerable<Comment> GetAll();
        BaseViewModel<Comment> GetByMovieId(long id);
    }
}