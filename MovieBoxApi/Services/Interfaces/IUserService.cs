﻿using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services.Interfaces
{
    public interface IUserService : IBaseService<User>
    {
    }
}