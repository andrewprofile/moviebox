﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services.Interfaces
{
    public interface ICategoryService : IBaseService<Category>
    {
        IEnumerable<Category> GetAll();
    }
}