﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.MovieViewModel;
using MovieBoxApi.Repository.Interfaces;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Services
{
    public class MovieService : BaseService<Movie>, IMovieService
    {
        protected readonly IMovieRepository MovieRepository;

        public MovieService(IMovieRepository repository) : base(repository)
        {
            MovieRepository = repository;
        }

        public override Movie CreateEntityFromViewModel(IBasicEntity item)
        {
            var model = (MovieViewModel) item;

            return new Movie
            {
                Id          = model.Id,
                Title       = model.Title,
                Description = model.Description,
                Duration    = model.Duration,
                Language    = model.Language,
                CategoryId  = model.CategoryId,
                Path        = model.Path
            };
        }

        public IEnumerable<Movie> GetAllMovie()
        {
            return MovieRepository.GetAllMovie();
        }

        public IEnumerable<Movie> GetAllTV()
        {
            return MovieRepository.GetAllTv();
        }

        public IEnumerable<Movie> GetAllGame()
        {
            return MovieRepository.GetAllGame();
        }
    }
}