﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.RateViewModel;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public sealed class RateController : BaseController<Rate, RateViewModel>
    {
        private readonly IRateService _service;

        public RateController(IRateService service) : base(service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet("{id}/movie")]
        public override BaseViewModel<Rate> Get(int id)
        {
            return _service.GetByMovieId(id);
        }

        [HttpGet("{id}/movie/{userId}")]
        public BaseViewModel<Rate> Get(int id, int userId)
        {
            return _service.GetByMovieIdAndUserId(id, userId);
        }
    }
}