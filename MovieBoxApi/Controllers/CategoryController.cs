using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.CategoryViewModel;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public sealed class CategoryController : BaseController<Category, CategoryViewModel>
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service) : base(service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return _service.GetAll();
        }
    }
}