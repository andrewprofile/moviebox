﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.AccountViewModel;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public sealed class AccountController : BaseController<User, UserViewModel>
    {
        public AccountController(IUserService service) : base(service)
        {
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public override BaseViewModel<User> Get(int id)
        {
            return base.Get(id);
        }

        [AllowAnonymous]
        [HttpPost]
        public override BaseViewModel Post([FromBody] UserViewModel item)
        {
            return base.Post(item);
        }

        [AllowAnonymous]
        [HttpPut]
        public override void Put([FromBody] UserViewModel item)
        {
            base.Put(item);
        }
    }
}
