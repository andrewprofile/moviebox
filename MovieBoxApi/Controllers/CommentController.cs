﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.CommentViewModel;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public sealed class CommentController : BaseController<Comment, CommentViewModel>
    {
        private readonly ICommentService _service;

        public CommentController(ICommentService service) : base(service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Comment> Get()
        {
            return _service.GetAll();
        }

        [AllowAnonymous]
        [HttpGet("{id}/movie")]
        public override BaseViewModel<Comment> Get(int id)
        {
            return _service.GetByMovieId(id);
        }
    }
}