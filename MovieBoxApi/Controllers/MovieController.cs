﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieBoxApi.Entity;
using MovieBoxApi.Models.MovieViewModel;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public sealed class MovieController : BaseController<Movie, MovieViewModel>
    {
        private readonly IMovieService _service;

        public MovieController(IMovieService service) : base(service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Movie> Get()
        {
            return _service.GetAllMovie();
        }

        [AllowAnonymous]
        [HttpGet("tv")]
        public IEnumerable<Movie> GetTv()
        {
            return _service.GetAllTV();
        }

        [AllowAnonymous]
        [HttpGet("game")]
        public IEnumerable<Movie> GetGame()
        {
            return _service.GetAllGame();
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public override BaseViewModel<Movie> Get(int id)
        {
            return base.Get(id);
        }
    }
}