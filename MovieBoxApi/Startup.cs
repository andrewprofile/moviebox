using System.Buffers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MovieBoxApi.Data;
using MovieBoxApi.Repository;
using MovieBoxApi.Repository.Interfaces;
using MovieBoxApi.Services;
using MovieBoxApi.Services.Interfaces;
using WebApi.Utils;
using WebApi.Utils.Auth;
using WebApi.Utils.Base;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            #region register repository
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ICommentRepository, CommentRepository>();
            services.AddTransient<IMovieRepository,   MovieRepository>();
            services.AddTransient<IRateRepository,    RateRepository>();
            services.AddTransient<IUserRepository,    UserRepository>();
            #endregion

            #region register application services
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IMovieService,   MovieService>();
            services.AddTransient<IRateService,    RateService>();
            services.AddTransient<IUserService,    UserService>();
            #endregion

            #region register framework services
            services.AddScoped<DbContext>(provider => provider.GetService<ApplicationContext>());

            services.AddEntityFramework()
                .AddDbContext<ApplicationContext>(options => options.UseSqlite(
                    Configuration.GetConnectionString("Database")
                )
            );

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                });
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new StatusCodeExceptionAttribute());
                options.Filters.Add(new WebServiceExceptionAttribute());
                options.OutputFormatters.Clear();

                var formatterSettings = JsonSerializerSettingsProvider.CreateSerializerSettings();
                formatterSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                var formatter = new JsonOutputFormatter(formatterSettings, ArrayPool<char>.Create());
                options.OutputFormatters.Add(formatter);
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMiddleware<CacheControlMiddleware>();
            app.UseMiddleware<JwtMiddleware>(new JwtOptions());
            app.UseCors("AllowAllOrigins");
            app.UseMvc();
        }
    }
}