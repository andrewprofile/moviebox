﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieBoxApi.Entity;
using MovieBoxApi.Repository.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Repository
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Comment> GetByMovieId(long id)
        {
            return Entities.Where(c => c.MovieId == id);
        }
    }
}