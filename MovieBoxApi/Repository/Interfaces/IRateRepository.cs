using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Repository.Interfaces
{
    public interface IRateRepository : IRepository<Rate>
    {
        double GetAvgByMovieId(long id);
        double GetByMovieIdAndUserId(int id, int userId);
    }
}