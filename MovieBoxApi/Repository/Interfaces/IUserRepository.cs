﻿using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Repository.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        User Get(User user);
        User GetByName(string username);
    }
}