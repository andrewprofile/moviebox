using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Repository.Interfaces
{
    public interface IMovieRepository : IRepository<Movie>
    {
        IEnumerable<Movie> GetAllMovie();
        IEnumerable<Movie> GetAllTv();
        IEnumerable<Movie> GetAllGame();
    }
}