﻿using System.Collections.Generic;
using MovieBoxApi.Entity;
using WebApi.Utils.Base.Interfaces;

namespace MovieBoxApi.Repository.Interfaces
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IEnumerable<Comment> GetByMovieId(long id);
    }
}