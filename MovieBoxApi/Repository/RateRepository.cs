﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieBoxApi.Entity;
using MovieBoxApi.Repository.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Repository
{
    public class RateRepository : Repository<Rate>, IRateRepository
    {
        public RateRepository(DbContext context) : base(context)
        {
        }

        public double GetAvgByMovieId(long id)
        {
            return Entities.Where( r => r.MovieId.Value == id )
                .GroupBy( g => g.MovieId.Value, r => r.Rating )
                .Select( r => r.Average() ).FirstOrDefault();
        }

        public double GetByMovieIdAndUserId(int id, int userId)
        {
            return Entities.Where(r => r.MovieId.Value == id && r.UserId.Value == userId)
                .GroupBy( g => g.MovieId.Value, r => r.Rating )
                .Select( r => r.First() ).FirstOrDefault();
        }
    }
}