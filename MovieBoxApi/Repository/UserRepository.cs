﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieBoxApi.Entity;
using MovieBoxApi.Repository.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public User Get(User user)
        {
            return Entities.FirstOrDefault(
                m => m.Username == user.Username && m.Password == user.Password
            );
        }

        public User GetByName(string username)
        {
            return Entities.FirstOrDefault(
                m => m.Username == username
            );
        }
    }
}