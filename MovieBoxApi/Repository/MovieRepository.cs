﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieBoxApi.Entity;
using MovieBoxApi.Repository.Interfaces;
using WebApi.Utils.Base;

namespace MovieBoxApi.Repository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Movie> GetAllMovie()
        {
            return Entities.Where(m => m.CategoryId == 1);
        }

        public IEnumerable<Movie> GetAllTv()
        {
            return Entities.Where(m => m.CategoryId == 2);
        }

        public IEnumerable<Movie> GetAllGame()
        {
            return Entities.Where(m => m.CategoryId == 3);
        }
    }
}