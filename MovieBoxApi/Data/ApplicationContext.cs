﻿using Microsoft.EntityFrameworkCore;
using MovieBoxApi.Entity;

namespace MovieBoxApi.Data
{
    public class ApplicationContext : DbContext
    {
        protected DbSet<Category> Category { get; set; }
        protected DbSet<Comment>  Comments { get; set; }
        protected DbSet<Movie>    Movies   { get; set; }
        protected DbSet<Rate>     Rates    { get; set; }
        protected DbSet<User>     Users    { get; set; }

        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }
    }
}